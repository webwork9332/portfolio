function initMap() {
  var uluru = {lat: 48.4278274, lng: -123.3628178};
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 12,
    center: uluru
  });
  var marker = new google.maps.Marker({
    position: uluru,
    map: map
  });
}